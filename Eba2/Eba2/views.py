from django.http import HttpResponse
from django.template.loader import get_template
from django.shortcuts import render


######## FUNCIONES ########

def medida(request, bath, room):
    precio = 0
    if(bath<1 or bath>2 or room<1 or room>4):
        texto="Las casas deben tener entre 1 a 2 baños y entre 1 a 4 habitaciones"
        formato="""
        <h1> Error: <br> %s </h1>
        """ % texto
        return HttpResponse(formato)
    precio = bath * 1000000 + room * 1500000
    archivo = get_template("medida.html")
    doc = archivo.render({"wc":bath,"piezas":room,"costo":precio})
    return HttpResponse(doc)

def metraje(request, metros):
    precio = 0
    if metros<50 :
        texto="La superficie minima debe ser 50m2"
        formato="""
        <h1> %s </h1>
        """ % texto
        return HttpResponse(formato)
    precio = metros * 33
    archivo = get_template("metraje.html")
    doc = archivo.render({"metros2":metros,"costo":precio})
    return HttpResponse(doc)

def gallery(request):
    return render(request, "galeria.html")

def index(request):
    return render(request, "index.html")

def contact(request):
    return render(request, "contacto.html")

def somos(request):
    return render(request, "quienes.html")
