***********************************************************
INTEGRANTES:
    Anaís Molina
    Oscar Poblete
    Javier Largo
***********************************************************

En el archivo settings.py, ubicar las siguientes variables:

1.-
STATICFILES_DIRS = ['path']

y cambiar 'path' por la ruta de la carpeta 'static'

EJ(que viene en el archivo):

STATICFILES_DIRS=['C:/Users/oscar/OneDrive/Escritorio/Prueba2/eva2/Eba2/Eba2/templates/static']


2.-

TEMPLATES = [
...
'DIRS': ['path'],
...
]

y cambiar 'path' por la ruta donde se encuentre la carpeta "templates"

EJ(que viene en el archivo):

'DIRS': ['C:/Users/oscar/OneDrive/Escritorio/Prueba2/eva2/Eba2/Eba2/templates'],